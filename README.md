# Golang REST service starter

This is an older starter project without database and
frontend. For the current Golang starter see [CSC Golang
SolidJS
Starter](https://bitbucket.org/ma14pace/csc-golang-solidjs-starter/).

We keep this project, because it contains things not found
elsewhere, for instance client certificates and usage of
`docker-compose-test.yml`. This makes for a somewhat
nonsensical functionality, that you likely won't need in
reality. In the most flagrant cases, comments will indicate,
that something is for demonstration of [Container Smart
CI](https://stp.wien.gv.at/container-smart-ci/docs/) only.

## Frameworks

Golang already has a remarkable number of relatively mature web
frameworks. Choices have to be made, and our current choices are these:

* [Echo](https://echo.labstack.com/) is the web framework
* [echo-swagger](https://github.com/swaggo/echo-swagger) is used for Swagger support


## Functionality provided

This is not a meaningful application in any way. There are a
handful of routes, and the handlers each show off the solution to a
specific problem. Additionally, there is a primitive configuration
mechanism. A docker configuration is provided as well.

### Configuration

[internal/util/config.go](svc/internal/util/config.go)
has a read method for a config file. It is called
in [cmd/svc/main.go](svc/cmd/svc/main.go) with a path
to a JSON file, either read from the commandline
or defaulting to `ci_test/test_config.json`. 

Route handlers are in
[internal/handlers](svc/internal/handlers).
There is also a file
[internal/handlers/types.go](svc/internal/handlers/types.go)
defining a struct `Env`, for now only
containing a reference to the configuration
read. Handlers are defined as methods of `Env`.

After reading the config, `main()` initializes the REST framework,
defines routes and handler and begins to listen for requests.

### Routes

#### Index (/v1/)

This is the "Hello World" of routes. It logs the access and returns
a JSON status of "OK".

#### Index (/v1/version)

In production, it returns the git version tag of the
project. The version comes from an environment variable
in the file `CURRENT_CONFIG_VERSION`. We mount this file
as `env_file`.

The supplied file is for development. It is overwritten
on the application server with either the semantic version
tag of the commit, or with `branch / short_hash` of the 
commit.

#### AccessHttpWithRedirectToHttps (/v1/http)

A [remote HTTP URL](http://www.wien.gv.at) is called, that we know to
permanently redirect to HTTPS. The client should follow the redirect
and we check that this actually happens. We wouldn't do that check
in practice, but here we can verify that a docker container "FROM
scratch" is not enough to run this program. It runs without problem
on the host, but inside an empty container, we'd get an error
validating the server's certificate. That means, we need at least
`/etc/ssl/certs` installed. 

#### UseClientCert (/v1/client_cert)

That's more of the same, but this time we call an HTTPS URL using a
client certificate. On success the URL returns a plain text body
with the certificate's identity.

We start out by reading the system certificate pool. Then we add the
CA certificate of CA that has signed the client cert. We go on
reading client cert and client cert key (unencrypted), create a
client and store it for further use.

### CheckEnvironment (/v1/env)

This one is a little bit silly, but we use it to test that
variables from environment files are correctly passed to
the service. The handler uses an `expected_env` from the
configuration, a string that can be one of `DEV`, `TEST`
or `PROD`. It checks its environment for Variables of that
name, and it expects the one with the name equal to the
value of `expected_env` to be `true`, the others `false`.

### AccessBackend (/v1/backend)

Here we demonstrate how an internal service `backend`, defined in
the same `docker-compose.yml`, can be called, with the host alias 
for the service and the exposed (as opposed to published) port.

The backend service is simulated using an [NGINX
container](https://hub.docker.com/_/nginx). It is defined with a
host alias of `backend.docker`. See
https://stp-test.wien.gv.at/docker-smart-ci/04_dev_view.html under
the heading "HTTP(S) Proxy" for details

## Docker

The supplied [Dockerfile](svc/Dockerfiles/svc/Dockerfile) makes a dual-stage build,
compiling the program in a builder image and installing in an image
__FROM scratch__. The resulting container is more or less the
minimum size achievable with Golang. The app is installed in `/app`.

[docker-compose.yml](docker-compose.yml) mounts [config](svc/config) as
a volume at `/app/config`. The directory [config](svc/config) initially
comes with only three files ending with `.j2`, indicating that these
files are Jinja2 templates filled in in production by Ansible.

## Building and testing

### Preparation

In order to test, copy
[config/config.json.j2](svc/config/config.json.j2) to
`config/config.json`. You will need a client certificate and its key
in PEM format. The key must not be encrypted. The following script
can generate CRT and KEY from a PKCS12 file.

```
#!/usr/bin/env bash

if [[ $# -ne 1 ]] ; then
    echo "usage $0 <certificate_p12>"
    exit 1
fi

STEM=$(basename $1 .p12)
if [[ ! -f ${STEM}.p12 ]] ; then
    echo "Expected input file ${STEM}.p12 not found or cannot determine stem from parameter $1"
    exit 1
fi

openssl pkcs12 -in ${STEM}.p12 -out ${STEM}.key -nocerts -nodes
openssl rsa -in ${STEM}.key -out ${STEM}_rsa.key
openssl pkcs12 -in ${STEM}.p12 -out ${STEM}.crt -clcerts -nokeys
mv ${STEM}_rsa.key ${STEM}.key

exit 0
```

Fill in `config/config.json`, choose a port and place CRT, KEY and a
PEM file with the CA certificate used to sign the key in
[config](svc/config).

For a quick test, the program can be started from the IDE. Then call
it with one of either 

```
curl -i -X GET http://localhost:3100/v1/
curl -i -X GET http://localhost:3100/v1/http
curl -i -X GET http://localhost:3100/v1/client_cert
curl -i -X GET http://localhost:3100/v1/env
curl -i -H 'X-Some-Request-Header: some-value' -X GET http://localhost:3100/v1/headers
curl -i -X GET http://localhost:3100/swagger/index.html
```

If running from the IDE, the run configuration must define 
environment variables like in [config/svc.env](svc/config/svc.env).

If routes have been added or handlers have been changed,
the swagger documentation has to be updated with 

```
swag init main.go -o internal/docs
```

### Creating the docker image

Throw away eventually present images and containers with 

```
docker-compose down --rmi all
```

and then start the application with 

```
docker-compose up
```

No image will be present and therefore it will be built. Please note
the special syntax in [docker-compose.yml](docker-compose.yml) for
the image tag. This syntax is required and will be replaced by
Jenkins when the images is produced in the CI server and eventually
pushed to the registry.

### Misc

Certificate creation and testssl server code have been liberally copied 
from [a gist by Nick Craig-Wood](https://gist.github.com/ncw/9253562).
