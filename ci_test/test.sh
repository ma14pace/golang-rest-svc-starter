#!/usr/bin/env bash


EXIT_CODE=0
for PORT in 3100 ; do
  for URL in \
      http://127.0.0.1:${PORT}/v1/ \
      http://127.0.0.1:${PORT}/v1/version \
      http://127.0.0.1:${PORT}/v1/http \
      http://127.0.0.1:${PORT}/v1/client_cert \
      http://127.0.0.1:${PORT}/v1/backend \
      http://127.0.0.1:${PORT}/v1/headers \
      http://127.0.0.1:${PORT}/swagger/index.html \
  ; do
      echo "### ${URL}"
      curl -si ${URL} | head -1 | grep 'HTTP/1.1 200 OK'
      EXIT_CODE=$(expr ${EXIT_CODE} + $?)
      if [[ ${EXIT_CODE} -gt 0 ]] ; then
          echo "Calling ${URL} failed!"
      fi
      echo
  done
done

if [[ ${EXIT_CODE} -eq 0 ]] ; then
    echo "### SUCCESS"
else
    echo "### FAILURE"
fi

exit "${EXIT_CODE}"
