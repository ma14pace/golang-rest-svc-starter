FROM golang:1.22-alpine as builder

# Install SSL ca certificates.
# Ca-certificates is required to call HTTPS endpoints.
# If the project needs CGO, alpine-sdk will be required as well
RUN apk update && apk add --update --no-cache git ca-certificates && update-ca-certificates
# Create appuser
RUN adduser -D -g '' appuser

WORKDIR /src/
COPY . .
# Fetch dependencies using go mod
RUN go mod download
# Run the unint tests; breaks the build if a test fails
RUN ./run_unit_tests.sh
# Build the binary
ENV GOOS=linux CGO_ENABLED=0
RUN go build -a -tags netgo,timetzdata -ldflags='-w -extldflags "-static"' -o /src/testssl


FROM scratch

WORKDIR /app

# Import from builder.
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /src/testssl /app/testssl

# Use an unprivileged user.
USER appuser

EXPOSE 3111

CMD ["/app/testssl"]
