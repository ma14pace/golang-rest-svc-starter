#!/bin/sh -x
#
# Determine all unique packages containing unit tests.
# Run all tests and fail if at least one test has failed.
#

EXIT_CODE=0
for p in $(
    find . -type f -a -name '*_test.go' -exec dirname {} \; | sort -u
    ) ;
do
    go test "${p}"
    if [ $? != 0 ] ; then EXIT_CODE=1 ; fi
done

exit ${EXIT_CODE}
