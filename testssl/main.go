// Taken from https://github.com/levigross/go-mutual-tls/blob/master/server/server.go
// Test certs crated after https://gist.github.com/Soarez/9688998
package main

import (
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"github.com/rs/zerolog/log"
	"io/ioutil"
	"net/http"
)

// HelloUser is a view that greets a user
func HelloUser(w http.ResponseWriter, req *http.Request) {
	_, _ = fmt.Fprintf(w, "Hello %v! \n", req.TLS.PeerCertificates[0].Subject)
}

func main() {
	cert, err := tls.LoadX509KeyPair(
		"/secrets/server.crt",
		"/secrets/server.key",
	)
	if err != nil {
		log.Printf("server: loadkeys: %s", err)
		panic(err)
	}
	certpool := x509.NewCertPool()
	pem, err := ioutil.ReadFile("/secrets/ca.pem")
	if err != nil {
		log.Printf("Failed to read client certificate authority: %v", err)
		panic(err)
	}
	if !certpool.AppendCertsFromPEM(pem) {
		log.Printf("Can't parse client certificate authority")
		panic(err)
	}

	tlsConfig := tls.Config{
		Certificates: []tls.Certificate{cert},
		ClientAuth:   tls.RequireAndVerifyClientCert,
		ClientCAs:    certpool,
	}
	tlsConfig.Rand = rand.Reader

	http.HandleFunc("/", HelloUser)

	httpServer := &http.Server{
		Addr:      ":3111",
		TLSConfig: &tlsConfig,
	}

	log.Print(httpServer.ListenAndServeTLS(
		"/secrets/server.crt", "/secrets/server.key"))
}
