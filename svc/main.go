package main

import (
	"context"
	"github.com/fatih/structs"
	_ "github.com/ghodss/yaml"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	_ "github.com/lib/pq"
	echoSwagger "github.com/swaggo/echo-swagger"
	_ "github.com/urfave/cli/v2"
	"golang-rest-svc-starter/internal/docs"
	"golang-rest-svc-starter/internal/handlers"
	"golang-rest-svc-starter/internal/util"
	_ "net/http"
	"os"
	"strings"
)

const configNameDefault = "/config/config.json"

var Logger echo.MiddlewareFunc

// @title Golang Rest Service Starter API
// @description This is as a starter project for Golang web services and a demo case for Docker Smart CI

// @contact.name PACE
// @contact.email pace@ma01.wien.gv.at

// @host stp-test.wien.gv.at:443
// @BasePath /golang-rest-svc-starter/svc

func main() {
	configName := configNameDefault
	if len(os.Args) == 2 {
		fileInfo, err := os.Stat(os.Args[1])
		if err == nil && fileInfo != nil && fileInfo.Mode().IsRegular() {
			configName = os.Args[1]
		}
	}
	cfg, err := util.ReadConfig(configName)
	if err != nil {
		log.Printf("Error reading configuration file: %s\n", err)
		panic(err)
	}

	log.Infoj(structs.Map(cfg))

	ctx := context.Background()

	version := os.Getenv("APP_VERSION")
	cfg.Version = version
	docs.SwaggerInfo.Version = version
	if len(version) == 0 || strings.HasPrefix(version, "development") {
		docs.SwaggerInfo.Host = "localhost:3100"
		docs.SwaggerInfo.BasePath = "/"
	}

	env := &handlers.Env{Cfg: cfg, Ctx: ctx}

	e := echo.New()

	LoggerConfig := middleware.DefaultLoggerConfig
	LoggerConfig.Format = `{"message":"${method} ${uri}, status=${status}", "time":"${time_rfc3339_nano}","id":"${id}","remote_ip":"${remote_ip}",` +
		`"host":"${host}","method":"${method}","uri":"${uri}","user_agent":"${user_agent}",` +
		`"status":${status},"error":"${error}","latency":${latency},"latency_human":"${latency_human}"` +
		`,"bytes_in":${bytes_in},"bytes_out":${bytes_out}}` + "\n"

	// Middleware
	Logger = middleware.LoggerWithConfig(LoggerConfig)
	e.Use(Logger)
	e.Use(middleware.Recover())

	v1 := e.Group("/v1")
	{
		v1.GET("/", env.Index)
		v1.GET("/version", env.Version)
		v1.GET("/headers", env.Headers)
		v1.GET("/http", env.AccessHttpWithRedirectToHttps)
		v1.GET("/client_cert", env.UseClientCert)
		v1.GET("/backend", env.AccessBackend)
	}
	e.GET("/swagger/*any", echoSwagger.WrapHandler)

	e.Logger.Fatal(e.Start(":3100"))

	os.Exit(0)
}
