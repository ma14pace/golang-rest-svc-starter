package model

type Version struct {
	Version string
}

type Status struct {
	StatusCode int
}

type Environment struct {
	Name string
}

type Message struct {
	Message string
}
