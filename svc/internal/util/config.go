package util

import (
	"encoding/json"
	"io/ioutil"
)

// Config gets read at startup.
type Config struct {
	ClientCertCrt        string `json:"client_cert_crt"`
	ClientCertKey        string `json:"client_cert_key"`
	CaCertPem            string `json:"ca_cert_pem"`
	ValidateCerts        bool   `json:"validate_certs"`
	ClientCertUrl        string `json:"client_cert_url"`
	BackendUrl           string `json:"backend_url"`
	HttpUrl              string `json:"http_url"`
	HttpVerificationText string `json:"http_verification_string"`
	Version              string `json:"-"`
}

// c is a global variable, that is read by the handlers
var c Config

// ReadConfig de-serializes the configuration from a JSON file
func ReadConfig(filename string) (*Config, error) {

	c = Config{}

	file, err := ioutil.ReadFile(filename)
	if err == nil {
		err = json.Unmarshal([]byte(file), &c)
	}
	return &c, err
}
