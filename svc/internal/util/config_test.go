package util

import (
	"testing"
)

func TestReadConfig(t *testing.T) {
	configFile := "../../test_config.json"

	_, err := ReadConfig(configFile)
	if err != nil {
		t.Fatalf("can't read config %s: %s", configFile, err.Error())
	}
}
