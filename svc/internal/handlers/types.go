package handlers

import (
	"context"
	"golang-rest-svc-starter/internal/util"
)

type Env struct {
	Cfg *util.Config
	Ctx context.Context
}
