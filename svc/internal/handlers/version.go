package handlers

import (
	"github.com/labstack/echo/v4"
	"golang-rest-svc-starter/internal/model"
	"net/http"
)

// Version godoc
// @Summary returns the version as configured
// @Description The returned version is set via ansible at install time
// @Produce json
// @Success 200 {object} model.Version
// @Router /v1/version [get]
func (env *Env) Version(c echo.Context) error {
	return c.JSON(http.StatusOK, model.Version{Version: env.Cfg.Version})
}
