package handlers

import (
	"errors"
	"github.com/labstack/echo/v4"
	"golang-rest-svc-starter/internal/model"
	"io/ioutil"
	"net/http"
	"strings"
)

//
// AccessHttpWithRedirectToHttps godoc
// @Summary accesses an external service via HTTP, that redirects to HTTPS
// @Description This is a demonstration of calling an external service redirecting to HTTPS.
//              The returned result is simply the status code of the backend call
// @Produce json
// @Success 200 {object} model.Status
// @Failure 500 {object} int
// @Router /v1/http [get]
func (env *Env) AccessHttpWithRedirectToHttps(c echo.Context) error {
	res, err := http.Get(env.Cfg.HttpUrl)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}
	if res.StatusCode != http.StatusOK || res.Request.Response.StatusCode != http.StatusMovedPermanently {
		return echo.NewHTTPError(http.StatusInternalServerError,
			errors.New("should have been redirected to HTTPS"))
	}

	defer func() { _ = res.Body.Close() }()

	if res.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err)
		}
		bodyString := string(bodyBytes)
		if !strings.Contains(bodyString, env.Cfg.HttpVerificationText) {
			return echo.NewHTTPError(http.StatusInternalServerError,
				errors.New("body does not contain verification text"))
		} else {
			c.Logger().Printf("Received response from %s", env.Cfg.HttpUrl)
		}
	}

	return c.JSON(res.StatusCode, model.Status{StatusCode: res.StatusCode})
}
