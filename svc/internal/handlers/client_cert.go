package handlers

import (
	"crypto/tls"
	"crypto/x509"
	"github.com/labstack/echo/v4"
	"golang-rest-svc-starter/internal/model"
	"golang-rest-svc-starter/internal/util"
	"io/ioutil"
	"net/http"
)

var tlsClient *http.Client = nil

// getTlsClient reads certs and CA certs from files in order to prepare a transport for the HTTP client
func getTlsClient(c echo.Context, cfg *util.Config) (*http.Client, error) {
	if tlsClient == nil {

		// Get the SystemCertPool, continue with an empty pool on error
		pool, _ := x509.SystemCertPool()
		if pool == nil {
			c.Logger().Print("No sytem cert pool, using empty pool")
			pool = x509.NewCertPool()
		}

		// the CertPool wants to add a root as a []byte so we read the file ourselves
		caCert, err := ioutil.ReadFile(cfg.CaCertPem)
		if err != nil {
			c.Logger().Printf("Can't read %s", cfg.CaCertPem)
			return nil, err
		}
		pool.AppendCertsFromPEM(caCert)

		// LoadX509KeyPair reads files, so we give it the paths
		clientCert, err := tls.LoadX509KeyPair(cfg.ClientCertCrt, cfg.ClientCertKey)
		if err != nil {
			c.Logger().Printf("Can't read %s or %s", cfg.ClientCertCrt, cfg.ClientCertKey)
			return nil, err
		}

		tlsConfig := tls.Config{
			RootCAs:            pool,
			Certificates:       []tls.Certificate{clientCert},
			InsecureSkipVerify: !cfg.ValidateCerts,
		}
		transport := http.Transport{
			TLSClientConfig: &tlsConfig,
		}

		// finally create the client
		tlsClient = &http.Client{
			Transport: &transport,
		}
	}
	return tlsClient, nil
}

// UseClientCert godoc
// @Summary accesses an external service via HTTPS
// @Description This is a demonstration of calling an external service using a
//              client certificate. The returned result is simply the
//              status code of the backend call
// @Produce json
// @Success 200 {object} model.Status
// @Failure 500 {object} int
// @Router /v1/client_cert [get]
func (env *Env) UseClientCert(c echo.Context) error {
	client, err := getTlsClient(c, env.Cfg)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}
	res, err := client.Get(env.Cfg.ClientCertUrl)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	defer func() { _ = res.Body.Close() }()

	if res.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err)
		}
		bodyString := string(bodyBytes)
		c.Logger().Print("Received response body [", bodyString, "]")
	}

	return c.JSON(res.StatusCode, model.Status{StatusCode: res.StatusCode})
}
