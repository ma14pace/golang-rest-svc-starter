package handlers

import (
	"errors"
	"fmt"
	"github.com/labstack/echo/v4"
	"golang-rest-svc-starter/internal/model"
	"io/ioutil"
	"net/http"
	"strings"
)

// AccessBackend godoc
// @Summary accesses an internal (NGINX) backend
// @Description This is a demonstration of calling an internal backend service. The returned result
//              is simply the status code of the backend call
// @Produce json
// @Success 200 {object} model.Status
// @Failure 500 {object} int
// @Router /v1/backend [get]
func (env *Env) AccessBackend(c echo.Context) error {
	res, err := http.Get(env.Cfg.BackendUrl)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}
	if res.StatusCode >= http.StatusBadRequest {
		return echo.NewHTTPError(http.StatusInternalServerError,
			errors.New(fmt.Sprintf("received status %s", res.Status)))
	}

	defer func() { _ = res.Body.Close() }()

	bodyBytes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}
	bodyString := string(bodyBytes)
	if !strings.Contains(bodyString, "It's safer here!") {
		return echo.NewHTTPError(http.StatusInternalServerError,
			errors.New("wrong content, maybe wrong server"))
	} else {
		c.Logger().Printf("Received response from %s", env.Cfg.BackendUrl)
	}

	return c.JSON(http.StatusOK, model.Status{StatusCode: res.StatusCode})
}
