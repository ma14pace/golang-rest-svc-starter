package handlers

import (
	"github.com/labstack/echo/v4"
	"golang-rest-svc-starter/internal/model"
	"net/http"
)

// Index godoc
// @Summary use this for a hello
// @Description Index is the handler for general information.
//              In a later version it will return user information obtained via auth methods
// @Produce json
// @Success 200 {object} model.Message
// @Router /v1/ [get]
func (env *Env) Index(c echo.Context) error {
	return c.JSON(http.StatusOK, model.Message{Message: "OK"})
}
