package handlers

import (
	"github.com/labstack/echo/v4"
	"net/http"
)

//
// Headers godoc
// @Summary returns the HTTP headers
// @Description use this to inspect the headers set by the portal and received by the service
// @Produce json
// @Success 200 {object} http.Header
// @Router /v1/headers [get]
func (env *Env) Headers(c echo.Context) error {
	return c.JSON(http.StatusOK, c.Request().Header)
}
